(Tiny/Micro)Core Linux Package Manager

An improved package manager for the (Tiny)Core Linux distro.

Released under the MIT license. See LICENSE for details.
