/*
	corepkg.c
	Package Manager for (Tiny)Core Linux
	Written By Martin A. COLEMAN (www.martincoleman.com)
    Released under the MIT license. See LICENSE for details.

Version history
v0.1 2012-01-16
* Basic shell of program.
* Checked for existence of all md5 files, re-generated them if any were missing.
* Downloads and verifies MD5 of package database.
* Embedded SQLite3
* Retrieves mirror information from /opt/tcemirror
* Creates master packages database.

v0.2 2012-01-17
* Package installer (just a wrapper to tce-load).
* Matches pkg db md5s against those on the system to see if files have been updated.

v0.3 2012-01-23
* Implemented missing dependency check.
* Implemented changes to accomodate my future Linux distro based on Core Linux.

v0.4 2012-01-24
* Made the dependency check recursive.

v0.5 2012-01-28
* Added simple command line parameters for install,fetch and upgrade.

v0.6 2012-01-29
* Placed real LINUX_MIRROR setting into separate header file with different licence.
* Tidied up pkgs_folder usage for source publication.
* Fixed up handling of old packages before new one is downloaded.

v0.7 2012-01-29
* Implement fetch DB and upgrade packages in one command "corepkg go"
* Implement packages list.
* Fixed a small bug in recursive dependency package checking targetting specific kernel versions.
* Fixed a small bug concerning single quotes in description in info files for sqlite3.
* Reset package database before re-creating it.

v0.8 2012-02-03
* Implemented keyword search. Borrowed this from coresome.

v0.81 2012-02-05
* Fixed minor bug with checking for pkg.db checksum.

v0.82 2012-02-11
* Is now a part of Nucleus Desktop, after the TinyCore Linux maintainer told me that their project
to make a modular, flexible Linux that could be used by anyone is not available to everyone nor
can they build modular, flexible projects with it. Ho-hum.
* Renamed all references to tce-load to my own installer.
* Changes all references from tcz to pkg.

v0.83 2012-06-08
* Re-released as open source on github.
* Users can re-enable the tce-load replacement quite easily.
* Added PKG_EXT definition for customisers.

v0.83a 2013-04-06
* Re-licensed under BSD 2 clause license.

v0.83b 2014-04-15
* Dedicated to the public domain. See UNLICENSE for details.

	pkg.db Basic DB structure
	CREATE TABLE packages (
	pkgname VARCHAR(64) NOT NULL,
	md5sum VARCHAR(128) NOT NULL,
	desc VARCHAR(128) NOT NULL,
	info TEXT);

v0.83c 2017-07-24
* Re-released under the MIT license.

NOTES:
There are some major, major hacks in this, but overall it works, and works
kind of damn well, at least from memory, but I haven't run it in like 3-4
months, so I have no idea how it will cope with TCL as it is.

DEV NOTES:
make
or
make client
for the client.
make server
for the server.

Updated to work on modern Linux with SQLite 3.8.xx
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include "sqlite3.c"

#define YEAR ((((__DATE__ [7] - '0') * 10 + (__DATE__ [8] - '0')) * 10 \
+ (__DATE__ [9] - '0')) * 10 + (__DATE__ [10] - '0'))

#define MONTH (__DATE__ [2] == 'n' ? 0 \
: __DATE__ [2] == 'b' ? 1 \
: __DATE__ [2] == 'r' ? (__DATE__ [0] == 'M' ? 2 : 3) \
: __DATE__ [2] == 'y' ? 4 \
: __DATE__ [2] == 'n' ? 5 \
: __DATE__ [2] == 'l' ? 6 \
: __DATE__ [2] == 'g' ? 7 \
: __DATE__ [2] == 'p' ? 8 \
: __DATE__ [2] == 't' ? 9 \
: __DATE__ [2] == 'v' ? 10 : 11)

#define DAY ((__DATE__ [4] == ' ' ? 0 : __DATE__ [4] - '0') * 10 \
+ (__DATE__ [5] - '0'))

#define DATE_AS_INT (((YEAR - 2000) * 12 + MONTH) * 31 + DAY)

#define LINUX_MIRROR "http://files.nucleus-desktop.info/"
#define COREPKG_DOWNLOAD "http://files.nucleus-desktop.info/"

#define VERSION "0.83a"
#define PKG_DB "pkg.db"
#define PKG_DB_CHK PKG_DB ".md5"
#define PKG_EXT ".pkg"

/* for installer. modify these back to TinyCore Linux settings for that distro. */
#define PKGSDIR "/tmp/pkgs"
#define PKGSLOOP "/tmp/pkgsloop"
//#define PKGSINSTALLED "/usr/local/pkgs.installed"
#define PKGSINSTALLED "/tmp/pkgs.installed"

/* stuff for tinycore linux */
char *pkgs_folder;
char pkgs_mirror[128];
char kernel_version[]="3.2.5-nucleus";	/* change this to whatever kernel base you are working with. from "uname -r" */

/* stuff for sqlite3 */
sqlite3 *db;
sqlite3_stmt *res;
char *zErrMsg=0;
int rc;

int force=0; /* override */

char *remove_ext1(char *filename)
{
	char *filename_without_extension;
	char *ext_start;
	
	filename_without_extension = filename;
	filename_without_extension = strdup(filename_without_extension);
	if((ext_start = strrchr(filename_without_extension, '.')))
		*ext_start=0;

	return filename_without_extension;
}

/* not mine: public domain */
char *replace_str(char *str, char *orig, char *rep)
{
  static char buffer[4096];
  char *p;

  if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
    return str;

  strncpy(buffer, str, p-str); // Copy characters from 'str' start to 'orig' st$
  buffer[p-str] = '\0';

  sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));

  return buffer;
}

/* this is not mine */
/* http://www.anyexample.com/programming/c/how_to_load_file_into_memory_using_plain_ansi_c_language.xml */
int ae_load_file_to_memory(const char *filename, char **result) 
{ 
	int size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL) 
	{ 
		*result = NULL;
		return -1; // -1 means file opening fail 
	} 
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*result = (char *)malloc(size+1);
	if (size != fread(*result, sizeof(char), size, f)) 
	{ 
		free(*result);
		return -2; // -2 means file reading fail 
	} 
	fclose(f);
	(*result)[size] = 0;
	return size;
}

/* utility functions found in the public domain, borrowed from my WikiCard project */
void chomp(char *s)
{
    while(*s && *s != '\n' && *s != '\r') s++;

    *s = 0;
}

char* rtrim(char* string, char junk)
{
    char* original = string + strlen(string);
    while(*--original == junk);
    *(original + 1) = '\0';
    return string;
}

char* ltrim(char *string, char junk)
{
    char* original = string;
    char *p = original;
    int trimmed = 0;
    do
    {
        if (*original != junk || trimmed)
        {
            trimmed = 1;
            *p++ = *original;
        }
    }
    while (*original++ != '\0');
    return string;
}

int file_exists(char *filename)
{
    FILE *fp;

    if(fp = fopen(filename, "r"))
    {
        fclose(fp);
        return 1;
    }
	return 0;
}
/* end of utility functions from WikiCard */

char *readlink_malloc (const char *filename)
          {
            int size = 100;
            char *buffer = NULL;
          
            while (1)
              {
                // buffer = (char *) xrealloc (buffer, size);
                buffer = (char *) malloc (size);
                int nchars = readlink (filename, buffer, size);
                if (nchars < 0)
                  {
                    free (buffer);
                    return NULL;
                  }
                if (nchars < size)
                  return buffer;
                size *= 2;
              }
          }

int download_file(char *filename)
{
	FILE *fpipe;
	char tmp_cmd[128];

	printf("Downloading %s\n", filename);
	sprintf(tmp_cmd, "wget -c %s%s 2>/dev/null", LINUX_MIRROR, filename);
	//fpipe=popen(tmp_cmd, "r"); pclose(fpipe);
	//printf("DEBUG: %s\n", tmp_cmd);
	system(tmp_cmd);
	return 0;
}

/*
int tce_load_file(char *filename)
{
	FILE *fpipe;
	char tmp_cmd[128];

	sprintf(tmp_cmd, "tce-load -w %s", filename);
	fpipe=popen(tmp_cmd, "r"); pclose(fpipe);
	return 0;
}
*/

int get_ext_acc(char *fname, char *ext)
{
	char ext_name[32];
	/*
	strcpy(ext_name, fname);
	strcat(ext_name, ext);
	*/
	sprintf(ext_name, "%s%s", fname, ext);
	if(!file_exists(ext_name))
	{
		download_file(ext_name);
	}
	return 0;
}

int remove_file_acc(char *fname, char *ext)
{
	char ext_name[32];
	/*
	strcpy(ext_name, fname);
	strcat(ext_name, ext);
	*/
	sprintf(ext_name, "%s%s", fname, ext);
	if(!file_exists(ext_name))
	{
		return 0;
	} else {
		remove(ext_name);
	}
	return 0;
}

int get_mirror()
{
	/*
		FILE *fp;
	
		if((fp=fopen("/opt/tcemirror", "r"))==NULL)
		{
			return 0;
		}
		fscanf(fp, "%[^\n]", pkgs_mirror);
		printf("Using pkg mirror: %s\n", pkgs_mirror);
		fclose(fp);
	*/
	sprintf(pkgs_mirror, "%s", LINUX_MIRROR);
	return 1;
}

void update_system(char *progname)
{
	char tmp_cmd[64];
	FILE *fp;

	system("sudo /sbin/ldconfig 2>/dev/null");
	// system("
	sprintf(tmp_cmd, "%s/%s", PKGSINSTALLED, progname);
	fp=fopen(tmp_cmd, "w"); fprintf(fp, "1"); fclose(fp);
}

/* this replaces tce-load */
int real_install_program(char *pkg_name)
{
	char tmp_cmd[64];
	tmp_cmd[0]='\0';
	char make_loop[64];
	char mount_cmd[64];
	char cp_to_fs[64];
	char *installedname;
	char already_installed[64]={0};

	/* prepare filenames */
	installedname=remove_ext1(pkg_name); // remove_ext(pkg_name, '.', '/');
	sprintf(already_installed, "%s/%s", PKGSINSTALLED, installedname);
	if(file_exists(already_installed))
	{
		printf("%s has already been installed.\n", pkg_name);
		return 1;
	}

	/* prepare installedname
	installedname[0]=' ';
	installedname[1]=' ';
	clear_name(installedname);
	*/

	/* perform the install */
	sprintf(make_loop, "%s/%s", PKGSLOOP, installedname);
	// printf("DEBUG: %s\n", make_loop);
	mkdir(make_loop, 0755);
	if(chdir(make_loop) == -1)
	{
		printf("Error creating loop directory.\n");
		return 1;
	}
	sprintf(mount_cmd, "sudo mount %s/%s %s/%s -t squashfs -o loop,ro,bs=4096 2>&1", PKGSDIR, pkg_name, PKGSLOOP, installedname);
	// printf("DEBUG: %s\n", mount_cmd);
	system(mount_cmd);
	sprintf(cp_to_fs, "yes n | sudo cp -ais %s/%s/* / 2>/dev/null", PKGSLOOP, installedname);
	// printf("DEBUG: %s\n", cp_to_fs);

	/* finish up */
	system("sudo /sbin/ldconfig 2>/dev/null");
	sprintf(tmp_cmd, "echo '1' > %s/%s", PKGSINSTALLED, installedname);
	// printf("DEBUG: %s\n", tmp_cmd);
	system(tmp_cmd);

	/* clean up */
	tmp_cmd[0]='\0';
	free(installedname);

	return 0;
	return 0;
}

int install_program(char *progname)
{
	FILE *fpipe;
	char tmp_cmd[128];

	if(!strstr(progname, PKG_EXT))
	{
		strcat(progname, PKG_EXT);
	}
	printf("Installing %s...", progname);
	sprintf(tmp_cmd, "%s/%s", PKGSDIR, progname);
	if(!file_exists(tmp_cmd))
	{
		putchar('.');
		download_file(progname);
	}
	if(!file_exists(tmp_cmd))
	{
		printf("Error retrieving file. Please try again later.\n");
		return 1;
	}
	tmp_cmd[0]='\0';
	sprintf(tmp_cmd, "pkginstall %s", progname);
	// real_install_program(progname);
	//fpipe=popen(tmp_cmd, "r"); pclose(fpipe);
	putchar('.');
	system(tmp_cmd);
	printf("Done.\n");
	return 0;
}

int fetch_pkg_db()
{
	FILE *fpipe;
	FILE *fp;
	char tmp_cmd[128];
	char md5_checksum[128];
	char tmp_md5[128];

	printf("Updating package database...\n");
	//chdir(pkgs_folder);
	/* if it already exists, delete it */
	if(file_exists(PKG_DB))
	{
		printf("Removing existing database.\n");
		if( remove(PKG_DB) != 0)
		{
			printf("Error deleting package database.\n");
			return 1;
		}
	}
	if(file_exists(PKG_DB))
	{
		if( remove(PKG_DB_CHK) != 0)
		{
			printf("Error deleting package database checksum.\n");
			return 1;
		}
	}
	printf("Downloading package database...");
	sprintf(tmp_cmd, "wget -c %s%s 2>/dev/null", COREPKG_DOWNLOAD, PKG_DB);
	#ifdef DEBUG
		printf("\n(DEBUG: [%s])\n", tmp_cmd);
	#endif
	fpipe=popen(tmp_cmd, "r"); pclose(fpipe);
	if(!file_exists(PKG_DB))
	{
		printf("Error downloading package database.\n");
		return 1;
	}
	printf("Done\n");
	
	/* let's verify the md5 checksum */
	tmp_cmd[0]='\0';
	sprintf(tmp_cmd, "wget -c %s%s 2>/dev/null", COREPKG_DOWNLOAD, PKG_DB_CHK);
	printf("Verifying package database...");
	fpipe=popen(tmp_cmd, "r"); pclose(fpipe);
	#ifdef DEBUG
		printf("\n(DEBUG: [%s])\n", tmp_cmd);
	#endif

	if((fp=fopen(PKG_DB_CHK, "r")) == NULL)
	{
		printf("Error opening %s checksum file.\n", PKG_DB_CHK);
		return 1;
	}
	fscanf(fp, "%s %s[^\n]", md5_checksum, NULL);
	fclose(fp);
	#ifdef DEBUG
		printf("\n(DEBUG: Internal checksum [%s])\n", md5_checksum);
	#endif

	tmp_cmd[0]='\0';
	sprintf(tmp_cmd, "md5sum %s", PKG_DB);
	fpipe=popen(tmp_cmd, "r");
	fscanf(fpipe, "%s %s[^\n]", tmp_md5, NULL);
	pclose(fpipe);
	printf("Done\n");
	#ifdef DEBUG
		printf("\n(DEBUG: Internal checksum [%s])\n", md5_checksum);
		printf("\n(DEBUG: External checksum [%s])\n", tmp_md5);
	#endif
	tmp_cmd[0]='\0';

	if(strcmp(tmp_md5, md5_checksum))
	{
		printf("An error occurred. The file may be corrupted. Please try again in a few minutes.\n");
		printf("If this continues after a few minutes, please contact the mirror administrator.\n");
	} else {
		printf("Everything looks good!\n");
	}
	return 0;
}

int get_record(char *pkgname, char ext_md5[128])
{
	char sql_str[1024];
	int result=0;

	sprintf(sql_str, "SELECT md5sum FROM packages WHERE pkgname='%s' LIMIT 1", pkgname);
	rc = sqlite3_open(PKG_DB, &db);
	if(rc)
	{
		printf("Can't open package database.");
		sqlite3_close(db);
		return 0;
	}
	rc = sqlite3_prepare_v2(db, sql_str, 1024, &res, 0);
	if(rc != SQLITE_OK)
	{
		printf("The package database file is corrupt!");
		sqlite3_free(zErrMsg);
		sqlite3_close(db);
		return 0;
	}
	while(1)
	{
		result=sqlite3_step(res);
		if(result==SQLITE_ROW)
		{
			sprintf(ext_md5, "%s",sqlite3_column_text(res, 0));
			//printf("DEBUG: GR 1[%s]\n", sqlite3_column_text(res, 0));
			//printf("DEBUG: GR 2[%s]\n", ext_md5);
		} else {
			break;
		}
	}
	sqlite3_finalize(res);
	sqlite3_close(db);
	return 1;
}

int insert_record(char *pkgname, char *md5sum, char *desc, char *info)
{
	/* char sql_str[4096];
	sql_str[0]='\0'; */
	char *sql_str;
	int result=0;

	#ifdef DEBUG
		printf("Recording %s...", pkgname);
	#endif
	sql_str=sqlite3_mprintf("INSERT INTO packages(pkgname, md5sum, desc, info) VALUES('%s', '%s', '%q', '%q')", pkgname, md5sum, desc, info);
	// sprintf(sql_str, "INSERT INTO packages(pkgname, md5sum, desc, info) VALUES('%s', '%s', '%s', '%q')", pkgname, md5sum, desc, info);

	rc = sqlite3_prepare_v2(db, sql_str, -1, &res, 0);
	if(rc != SQLITE_OK)
	{
		printf("DB Error: %s\n", sqlite3_errmsg(db));
		sqlite3_free(zErrMsg);
		sqlite3_close(db);
		return 1;
	}
	while(1)
	{
		result=sqlite3_step(res);
		if(result==SQLITE_ROW)
		{
		} else {
			break;
		}
	}
	sqlite3_finalize(res);

	#ifdef DEBUG
		printf("[%s]\n", sql_str);
	#endif
	sql_str[0]='\0';
	return 0;
}

int list_pkgs(void)
{
	char sql_str[1024];
	int result=0;

	printf("PKG DB listing.\n");
	sprintf(sql_str, "SELECT pkgname, desc FROM packages");
	rc = sqlite3_open(PKG_DB, &db);
	if(rc)
	{
		printf("Can't open package database.");
		sqlite3_close(db);
		return 0;
	}
	rc = sqlite3_prepare_v2(db, sql_str, 1024, &res, 0);
	if(rc != SQLITE_OK)
	{
		printf("The package database file is corrupt!");
		sqlite3_free(zErrMsg);
		sqlite3_close(db);
		return 0;
	}
	while(1)
	{
		result=sqlite3_step(res);
		if(result==SQLITE_ROW)
		{
			printf("%26s\t%s\n",sqlite3_column_text(res, 0), sqlite3_column_text(res, 1));
		} else {
			break;
		}
	}
	sqlite3_finalize(res);
	sqlite3_close(db);
	return 0;
}

int search_pkgs(char *keyword)
{
	char sql_str[1024];
	int result=0;
	int num_results=0;

	printf("PKG DB search for \"%s\".\n", keyword);
	sprintf(sql_str, "SELECT pkgname,desc FROM packages WHERE pkgname LIKE '%%%s%%' OR desc LIKE '%%%s%%' OR info LIKE '%%%s%%' ORDER BY pkgname ASC", keyword, keyword, keyword);
	rc = sqlite3_open(PKG_DB, &db);
	if(rc)
	{
		printf("Can't open package database.");
		sqlite3_close(db);
		return 0;
	}
	rc = sqlite3_prepare_v2(db, sql_str, 1024, &res, 0);
	if(rc != SQLITE_OK)
	{
		printf("The package database file is corrupt!");
		sqlite3_free(zErrMsg);
		sqlite3_close(db);
		return 0;
	}
	while(1)
	{
		result=sqlite3_step(res);
		if(result==SQLITE_ROW)
		{
			printf("%-30s\t%s\n",sqlite3_column_text(res, 0), sqlite3_column_text(res, 1));
			num_results++;
		} else {
			break;
		}
	}
	sqlite3_finalize(res);
	sqlite3_close(db);
	printf("Found %d results.\n", num_results);
	return 0;
}

#ifdef SERVER_SIDE
int reset_pkg_db(void)
{
	int result=0;

	rc = sqlite3_prepare_v2(db, "DELETE FROM packages", -1, &res, 0);
	if(rc != SQLITE_OK)
	{
		printf("Something is wrong with the package database file.");
		sqlite3_free(zErrMsg);
		sqlite3_close(db);
		return 0;
	}
	while(1)
	{
		result=sqlite3_step(res);
		if(result==SQLITE_ROW)
		{
			// nothing
		} else {
			break;
		}
	}
	sqlite3_finalize(res);
	return 0;
}

int generate_master_database()
{
	FILE *fp;
	DIR *d;
	struct dirent *dir;
	int filecnt=0;
	char str_md5[128];
	char file_md5[128];
	char *str_info;
	int size;
	char tmp_filename[128];
	char junk[20];
	char str_desc[128];
	char file_desc[128];
	char desc_str[128];

	printf("Re-Creating master package database.\n");
	rc = sqlite3_open(PKG_DB, &db);
	if(rc)
	{
		printf("Can't open package database.");
		sqlite3_close(db);
		return 1;
	}
	printf("Resetting package information.\n");
	reset_pkg_db();
	printf("Opening PKGS folder...");
	if(chdir(pkgs_folder))
	{
		printf("Error locating %s\n", pkgs_folder);
		return 1;
	} else {
		printf("Done\n");
	}
	d = opendir(pkgs_folder);
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			if(strstr(dir->d_name, PKG_EXT))
			{
				if(!strstr(dir->d_name, ".md5") && !strstr(dir->d_name, ".dep") && !strstr(dir->d_name, ".info") && !strstr(dir->d_name, ".lst"))
				// if(!strstr(dir->d_name, ".md5.txt") && !strstr(dir->d_name, ".dep") && !strstr(dir->d_name, ".info") && !strstr(dir->d_name, ".ztree") && !strstr(dir->d_name, ".lst"))
				{
					printf("Processing %s,", dir->d_name);
					/* get md5 checksum */
					sprintf(file_md5, "%s.md5", dir->d_name);
					fp=fopen(file_md5, "r");
					if(fp==NULL)
					{
						printf("Unable to open %s.\n", file_md5);
						break;
					}
					fscanf(fp, "%s %s[^\n]", str_md5, NULL);
					fclose(fp);
					printf("md5,");

					sprintf(file_desc, "%s.info", dir->d_name);
					fp=fopen(file_desc, "r");
					if(fp==NULL)
					{
						printf("Unable to open %s.\n", file_desc);
						break;
					}
					while(fgets(str_desc, sizeof(str_desc), fp) != NULL)
					{
				        	if(strstr(str_desc, "Description")) break;
					}
					sscanf(str_desc, "%s %[^\n]", junk, desc_str);
					fclose(fp);
					printf("desc,");
					
					size = ae_load_file_to_memory(file_desc, &str_info);
					if(size<0)
					{
						printf("Error loading %s file.\n", file_desc);
						goto finish_it_all;
					}
					str_desc[0]='\0';
					printf("info,");
					if(insert_record(dir->d_name, str_md5, desc_str, str_info)==1) break;
					printf("Done.\n");
					filecnt++;
				}
			}
		}
	} else {
		printf("Error opening %s\n", pkgs_folder);
	}
finish_it_all:
	sqlite3_close(db);
	closedir(d);
	printf("Complete.\n");
	printf("%d files processed.\n", filecnt);
	return 0;
}
#endif

/* Generic package code. This is not directly used, but it was
   used as a template for other functions here */
int update_db()
{
	DIR *d;
	struct dirent *dir;
	int filecnt=0;
	d = opendir(pkgs_folder);
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			if(strstr(dir->d_name, ".md5"))
			{
				printf("%s\n", dir->d_name);
				filecnt++;
			}
		}
		closedir(d);
		printf("%d files processed.\n", filecnt);
	}
	return 0;
}

int regenerate_md5()
{
	FILE *fpipe;
	DIR *d;
	struct dirent *dir;
	int filecnt=0;
	char tmp_filename[128];
	char tmp_cmd[128];

	tmp_filename[0]='\0';
	tmp_cmd[0]='\0';

	printf("Opening pkg directory...");
	//chdir(pkgs_folder);
	//printf("Done.\n");

	d = opendir(pkgs_folder);
	printf("Re-generating any needed md5 files.\n");
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			if(strstr(dir->d_name, PKG_EXT))
			{
				//if(!strstr(dir->d_name, ".md5.txt") && !strstr(dir->d_name, ".dep") && !strstr(dir->d_name, ".info") && !strstr(dir->d_name, ".ztree") && !strstr(dir->d_name, ".lst"))
				if(!strstr(dir->d_name, ".md5") && !strstr(dir->d_name, ".dep") && !strstr(dir->d_name, ".info") && !strstr(dir->d_name, ".lst"))
				{
					sprintf(tmp_filename, "%s.md5", dir->d_name);
					if(!file_exists(tmp_filename))
					{
						printf("%s missing, re-creating...", tmp_filename);
						sprintf(tmp_cmd, "md5sum %s > %s", dir->d_name, tmp_filename);
						if(!(fpipe=popen(tmp_cmd, "r")))
						{
							printf("Error generating md5 file for %s.\n", dir->d_name);
						} else {
							pclose(fpipe);
						}
						printf("Done.\n");
						// printf("DEBUG: md5 cmd: [%s]\n", tmp_cmd);
						filecnt++;
					}
					tmp_filename[0]='\0';
					tmp_cmd[0]='\0';
				}
			}
		}
		closedir(d);
		if(filecnt>0)
		{
			printf("%d new md5 files generated.\n", filecnt);
		} else {
			printf("All md5 files already exist.\n");
		}
	}
	return 0;
}

/* replaces tce-update */
int upgrade_pkgs()
{
	FILE *fp;
	DIR *d;
	struct dirent *dir;
	char file_md5[128];
	char ext_md5[128];
	int filecnt=0, files_tbd=0, files_proc=0;
	char tmp_str[128]; /* this is just wasted */
	ext_md5[0]='\0';
	char str_md5[128];
	str_md5[0]='\0';
	
	printf("Upgrading packages...\n");
	printf("Locating %s\n", pkgs_folder);
	//chdir(pkgs_folder);
	d = opendir(pkgs_folder);
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			if(strstr(dir->d_name, PKG_EXT))
			{
				// if(!strstr(dir->d_name, ".md5") && !strstr(dir->d_name, ".dep") && !strstr(dir->d_name, ".info") && !strstr(dir->d_name, ".ztree") && !strstr(dir->d_name, ".lst"))
				if(!strstr(dir->d_name, ".md5") && !strstr(dir->d_name, ".dep") && !strstr(dir->d_name, ".info") && !strstr(dir->d_name, ".lst"))
				{
					// printf("Checking %s\n", dir->d_name);
					files_proc++;
					fp=fopen(dir->d_name, "r");
					if(fp==NULL)
					{
						printf("Error opening file %s\n", dir->d_name);
						return 1;
					}
					fclose(fp);

					/* get md5 checksum */
					sprintf(file_md5, "%s.md5", dir->d_name);
					fp=fopen(file_md5, "r");
					fscanf(fp, "%s %s[^\n]", str_md5, tmp_str); tmp_str[0]='\0';
					//fgets(tmp_str, sizeof(tmp_str), fp);
					//str_md5=strtok(tmp_str, " ");
					fclose(fp);
					//printf("DEBUG: Local checksum [%s]\n", str_md5);
					
					get_record(dir->d_name, ext_md5);
					//printf("DEBUG: DB checksum [%s]\n", ext_md5);
					if(ext_md5==NULL)
					{
						closedir(d);
						printf("An error occurred retrieving the record for %s.\n", dir->d_name);
					}
					if(strlen(ext_md5)>1)
					{
						if(strcmp(str_md5, ext_md5)) /* are they different? */
						{
							printf("%s has been updated.\n", dir->d_name);
							files_tbd++;
							if(force)
							{
								/* remove old copy */
								remove(dir->d_name);
								remove_file_acc(dir->d_name, ".md5");
								remove_file_acc(dir->d_name, ".dep");
								//remove_file_acc(dir->d_name, ".lst");
								//remove_file_acc(dir->d_name, ".info");
								
								printf("Getting new files...\n");
								/* get new one */
								download_file(dir->d_name);
								filecnt++;
							}
						}
					}
				}
				ext_md5[0]='\0';
			}
		}
		closedir(d);
		printf("%d files processed.\n", files_proc);
		if(files_tbd>1)
		{
			if(filecnt<1)
			{
				printf("%d files awaiting upgrades.\n", files_tbd);
			} else {
				printf("Upgraded %d packages.\n", filecnt);
			}
		} else {
			printf("Your files are all up to date!\n");
		}
	}
	/*else {
		printf("Could not open TCE directory.\n");
	} */
	return 0;
}

/*
int rec_program_deps1(char *fname)
{
	char checkfile[32];

	strcpy(checkfile, fname);
	if(!file_exists(fname))
	{
		printf("Downloading %s...\n", fname);
		download_file(fname);
	}
}
*/

int rec_check_program_deps(char *progname)
{
	FILE *test_fp;
	FILE *fp;
	char fname[128];
	char flist[64];
	char real_name[64];

	if(strlen(progname)<2)
	{
		return 1;
	}
	// strcpy(fname, progname); /* the old way */
	sprintf(fname, "%s", replace_str(progname, "KERNEL", kernel_version));
	if(!strstr(fname, PKG_EXT))
	{
		strcat(fname, PKG_EXT);
	}

	if(!file_exists(fname))
	{
		download_file(fname);
		real_name[0]='\0';
	}
	if(force)
	{
		get_ext_acc(fname, ".md5");
		#ifdef SERVER_SIDE
		get_ext_acc(fname, ".lst");
		get_ext_acc(fname, ".info");
		#endif
	}

	printf("Checking all dependencies for %s.\n", fname);
	strcat(fname, ".dep");
	//printf("DEBUG: dep name: %s\n", fname);
	if(!file_exists(fname))
	{
		//printf("Downloading %s...\n", fname);
		download_file(fname);
	}
	if(file_exists(fname))
	{
		printf("Reviewing dependencies...\n");
		// fname[0]='\0';
		fp=fopen(fname, "r");
		while(fgets(flist, sizeof(flist), fp) != NULL)
		{
			chomp(flist);
			sprintf(real_name, "%s", replace_str(flist, "KERNEL", kernel_version));
			rec_check_program_deps(real_name);
			//check_program_deps(real_name);
			flist[0]='\0';
			real_name[0]='\0';
		}
		fclose(fp);
		printf("Dependencies completed.\n");
	}
	fname[0]='\0';
	printf("Extension completed.\n");
	return 0;
}

int check_program_deps(char *progname)
{
	char cCurrentPath[128];

	printf("Checking extensions and related dependencies...\n");
	if (!get_mirror())
	{
		printf("Error retrieving mirror info from /etc/corepkg/mirror. Perhaps the file does not exist?\n");
		return 1;
	}
	// printf("Using mirror %s\n", LINUX_MIRROR);
	rec_check_program_deps(progname);
	printf("Finished.\n");
	return 0;
}

/* run-time checking for testing. serves no practical purpose for users */
int debug_program()
{
	printf("Running good.\n");
	if(force)
	{
		printf("Override option is enabled.\n");
	}
	printf("Version %s.\nCompiled on %s at %s.\nBuild #%d.\n", VERSION, __DATE__, __TIME__, DATE_AS_INT);
	return 0;
}

int main(int argc, char *argv[])
{
	#ifdef SERVER_SIDE
	pkgs_folder=".";
	#else
		#ifdef STABLE
		/* modify this for your own distro */
		pkgs_folder=readlink_malloc("/etc/corepkg/pkgsdir");
		strcat(pkgs_folder, "/");
		// strcat(pkgs_folder, "/optional/");
		#else
		pkgs_folder="/tmp/pkgs/";
		#endif
	#endif

	printf("(Tiny)Core Package Manager v%s.\n", VERSION);
	printf("Copyright (C) 2012-2014 Martin COLEMAN. All rights reserved.\nReleased under the 2-clause BSD license.\n");
	#ifdef SERVER_SIDE
		printf("Supports server side operations.\n");
	#endif

	if(argc<2)
	{
print_help:
		printf("CorePkg paramters are:\n");
		printf("Basics: install, fetch, upgrade, list, search\n");
		printf("-f\t\tFetches the latest package database.\n");
		printf("-u\t\tUpgrades any applicable packages.\n");
		printf("-r\t\tRe-generates the md5 file for any packages missing it.\n");
		printf("-i package\tInstalls package. \"%s\" is optional.\n", PKG_EXT);
		printf("-v\t\tGet version and build information.\n");
		printf("\t[f]\tForce/Override/Verbose on certain options.\n");
		printf("-d program\tChecks to make sure you have all the dependencies of [package].\n");
		#ifdef SERVER_SIDE
		printf("-g\t\t(Server side) (Re-)Generates %s from the md5/info files.\n", PKG_DB);
		#endif
		return 0;
	}
	if(argc>1)
	{
		if(!strcmp(argv[1], "go"))
		{
			fetch_pkg_db();
			force=1;
			upgrade_pkgs();
			return 0;
		}
		if(!strcmp(argv[1], "search"))
		{
			if(argv[2])
			{
				search_pkgs(argv[2]);
			} else {
				printf("No keyword specified.\n");
			}
			return 0;
		}
		if(!strcmp(argv[1], "install"))
		{
			if(argv[2])
			{
				install_program(argv[2]);
			} else {
				printf("No program specified.\n");
			}
			return 0;
		}
		if(!strcmp(argv[1], "fetch"))
		{
			fetch_pkg_db();
			return 0;
		}
		if(!strcmp(argv[1], "upgrade"))
		{
			force=1;
			upgrade_pkgs();
			return 0;
		}
		if(!strcmp(argv[1], "list"))
		{
			list_pkgs();
			return 0;
		}
		if(argv[1][2])
		{
			if(argv[1][2]=='f') force=1;
		}
		if(argv[1][0]=='-')
		{
			switch(argv[1][1])
			{
				case 'i':
					if(argv[2])
					{
						install_program(argv[2]);
					} else {
						printf("No program specified.\n");
					}
					break;
				case 'd':
					if(argc<3)
					{
						printf("No program specified.\n");
						return 0;
					} else {
						if(strlen(argv[2]) >1)
						{
							check_program_deps(argv[2]);
						} else {
							printf("No program specified.\n");
							return 1;
						}
					}
					// debug_program();
					break;
				#ifdef SERVER_SIDE
				case 'g':
					generate_master_database();
					break;
				#endif
				case 'f':
					fetch_pkg_db();
					break;
				case 'r':
					regenerate_md5();
					break;
				case 'u':
					upgrade_pkgs();
					break;
				case 'l':
					list_pkgs();
					break;
				case 'v':
					printf("CorePkg v%s Build %d.\n", VERSION, DATE_AS_INT);
					printf("Original program by Martin COLEMAN (C) 2012.\n");
					break;
				default:
					printf("Unknown option.\n");
					goto print_help;
					break;
			}
		} else {
			printf("Unknown parameter.\n");
			goto print_help;
			return 1;
		}
	}
	return 0;
}
