CC=gcc
CFLAGS=-DSQLITE_OMIT_LOAD_EXTENSION -DSQLITE_DISABLE_LFS -DSQLITE_THREADSAFE=0 -DSQLITE_OS_UNIX=1 -DSQLITE_TEMP_STORE=1 -DHAVE_MREMAP=0

all: client

server: corepkg.c
	${CC} -DDEBUG -DSERVER_SIDE -o corepkg_svr corepkg.c ${CFLAGS}

client: corepkg.c
	${CC} -o corepkg corepkg.c ${CFLAGS}

